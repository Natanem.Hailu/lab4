package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

public class GridView extends JPanel {
    private IColorGrid grid;

    // Definerer konstanter
    private static final double OUTERMARGIN = 30;
    private static final Color MARGINCOLOR = Color.LIGHT_GRAY;
    private static final Color EMPTY_CELL_COLOR = Color.DARK_GRAY;

    public GridView(IColorGrid grid) {
        this.grid = grid;
        setPreferredSize(new Dimension(400, 300));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        // Tegner det store grå rektangelet som bakgrunn
        Rectangle2D outerBox = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, 
            getWidth() - 2 * OUTERMARGIN, getHeight() - 2 * OUTERMARGIN);
        g2d.setColor(MARGINCOLOR);
        g2d.fill(outerBox);

        
        CellPositionToPixelConverter converter = new CellPositionToPixelConverter(
            outerBox, grid, OUTERMARGIN);

        
        drawCells(g2d, grid, converter);
    }

    private static void drawCells(Graphics2D g2d, CellColorCollection collection, CellPositionToPixelConverter converter) {
        collection.getCells().forEach(cellColor -> {
            Rectangle2D cellBounds = converter.getBoundsForCell(cellColor.cellPosition());
            g2d.setColor(cellColor.color() != null ? cellColor.color() : EMPTY_CELL_COLOR);
            g2d.fill(cellBounds);
        });
    }
}
