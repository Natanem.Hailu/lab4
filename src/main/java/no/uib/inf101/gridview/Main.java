package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;

import javax.swing.JFrame;
import java.awt.Color;

public class Main {
  public static void main(String[] args) {
    
    IColorGrid grid = new ColorGrid(3, 4);
    
    
    grid.set(new CellPosition(0, 0), Color.RED);    // Øvre venstre
    grid.set(new CellPosition(0, 3), Color.BLUE);   // Øvre høyre
    grid.set(new CellPosition(2, 0), Color.YELLOW); // Nedre venstre
    grid.set(new CellPosition(2, 3), Color.GREEN);  // Nedre høyre
    
    
    GridView gridView = new GridView(grid);

   
    JFrame frame = new JFrame("Grid View");
    frame.setContentPane(gridView);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
}
}