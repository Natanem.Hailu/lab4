package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {
    private Color[][] grid;

    public ColorGrid(int rows, int cols) {
        grid = new Color[rows][cols];
    }

    @Override
    public Color get(CellPosition pos) {
        validatePosition(pos);
        return grid[pos.row()][pos.col()];
    }

    @Override
    public void set(CellPosition pos, Color color) {
        validatePosition(pos);
        grid[pos.row()][pos.col()] = color;
    }

    @Override
    public int rows() {
        return grid.length;
    }

    @Override
    public int cols() {
        return grid[0].length;
    }

    @Override
    public List<CellColor> getCells() {
        List<CellColor> cells = new ArrayList<>();
        for (int row = 0; row < rows(); row++) {
            for (int col = 0; col < cols(); col++) {
                // Inkluderer alle celler, også de som ikke har en farge satt (null).
                cells.add(new CellColor(new CellPosition(row, col), grid[row][col]));
            }
        }
        return cells;
    }

    private void validatePosition(CellPosition pos) {
        if (pos.row() < 0 || pos.row() >= rows() || pos.col() < 0 || pos.col() >= cols()) {
            throw new IndexOutOfBoundsException("Posisjon " + pos + " er utenfor rutenettet.");
        }
    }
}
